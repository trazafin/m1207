#include<stdio.h>
#include<math.h>

#define PI 3.141529

/*calcul du  sin et cos d'un angle entré par l'utilisateur*/

int main (void){
	float angle = 0;
	printf("Entrez un angle en deg: ");
	scanf("%f",&angle);
	printf("le cos de %.2f est: %.2f\n le sin de %.2f est: %.2f\n la racine carrée de %.2f est: %.2f\n ", 
		angle,
		cosf(PI*angle/180),
		angle,
		sinf(PI*angle/180),
		angle,
		sqrt(angle)
	);
	return 0;
}
