#include<stdio.h>
void init(int *p_i, int *p_j, float *p_k);
int main(void){
	int i, j;
	int *p_i, *p_j;
	p_i = &i;
	p_j = &j;
	float k;
	float *p_k;
	p_k = &k;
	init(p_i, p_j, p_k);
	printf("%i %i %f\n", i, j, k);
	return 0;
}

void init(int *p_i, int *p_j, float *p_k){
	*p_i = 10;
	*p_j = 20;
	*p_k = 3.141529;
}
