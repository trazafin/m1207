#include<stdio.h>

int main(void){
	int i = 0;
	int nb = 1;
	float tab[100];
	float somme = 0;

	printf("Entrez le nombre de note : ");
	scanf("%i", &nb);

	for(i = 0; i < nb ; i ++){
		printf("Entrez la note (%i) : ", i + 1);
		scanf("%f", &tab[i]);
	}
	for(i = 0; i < nb ; i ++){
		somme = somme + tab[i];
		printf("La note (%i) : %.2f\n", i,tab[i]);
	}
	printf("La moyenne des notes est : %.2f\n", somme/nb);
	return 0;
} 
