#include<stdio.h>

void affiche1(void);
void affiche2(int rep);
int affiche3(int rep);

int main (void){
	int repetition = 1;
	int retour = 1;
	printf("Nombre rep: ");
	scanf("%i",&repetition);
	affiche1();
	affiche2(repetition);
	retour = affiche3(repetition);
	if (retour == 0){
		printf("tout va bien\n");
	} else {
		printf("erreur dans affiche 3\n");
	}
	return 0;
}

void affiche1(void){
	printf("affiche 1\n");
}

void affiche2(int rep){
	int j =0;
	for (j=0; j<rep; j++){
		printf("salut affiche 2\n");
	}
}

int affiche3(int rep){
	int j =0;
	for (j=0; j<rep; j++){
		printf("salut affiche 3\n");
	}
	return 0;
}
