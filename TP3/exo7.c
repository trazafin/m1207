#include<stdio.h>

int main(void){
	int i = 0;
	int somme = 0;
	char string[]="Comment allez vous ?";
	
	while(string[i] != '\0'){
		printf("%c - %i \n", string[i],string[i]);
		somme = somme + (int)(string[i]);
		// dans la ligne précédente je fais un cast, 
		// je transforme une variable de type char en 
		// une variable de type int.
		i++;
	}
	printf("Somme vaut: %i \n", somme);
	return 0;
} 
