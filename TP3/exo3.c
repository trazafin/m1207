#include<stdio.h>

int main(void){
	int i = 0;
	float tab[5];
	float somme = 0;

	for(i = 0; i < 5 ; i ++){
		printf("Entrez la note (%i) : ", i + 1);
		scanf("%f", &tab[i]);
	}
	for(i = 0; i < 5 ; i ++){
		somme = somme + tab[i];
		printf("La note (%i) : %.2f\n", i,tab[i]);
	}
	printf("La moyenne des notes est : %.2f\n", somme/5);
	return 0;
} 
