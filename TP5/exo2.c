### scanf

La fonction scanf stocke la valeur entré par l'utilisateur à l'adresse de la variable se situant après la virgule.

La fonction scanf retourne le nombre d'entrées assignées.
